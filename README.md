# PlaFRIM CI Proxy for hwloc

hwloc is hosted on github, hence cannot be CI-tested on PlaFRIM
directly. This proxy project is a workaround to perform some
automatic testing (the gitlab pipeline is scheduled to run every
morning).

The main script is test-hwloc.sh:
* It looks at the latest stable build tarball from Jenkins (master branch).
  * If it's the same as the previous run, it stops.
    * Except if FORCE=1 is set in the environment,
      when manually running the pipeline.
* Then test-hwloc.sh submits several SLURM jobs on different
  kinds of sirocco nodes.
  * They execute test-hwloc-single.sh, which downloads the latest
    Jenkins tarball, builds and tests it.
  * On success, those jobs write SUCCESS on the last line of their output.
* The main script looks at those last lines to decide to
  return failure or success.
* The output of each job is also sent to stdout so that
  it's visible in the gitlab console.
* The job fails after one hour if no sirocco node could be found.
