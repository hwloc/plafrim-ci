#!/bin/sh

set -e

failed=0
failedlist=

tarballname=$(wget -O - 'https://ci.inria.fr/hwloc/job/basic/job/master/lastSuccessfulBuild/api/xml?tree=artifacts[relativePath]' | sed -r -n -e 's/^.*>(.+\.tar\.bz2)<.*$/\1/p')
commit=$CI_COMMIT_SHA
test_id=${tarballname}"-on-"${commit}
test_id_file=$HOME/.latest_test_id

if test x$test_id == x$(cat $test_id_file) -a x$FORCE != x1; then
  echo "$tarballname already tested with $commit, ignoring"
  # should rather be a SKIP return code?
  exit 0
fi
echo "Testing tarball $tarballname ..."

echo "Submitting job ..."
job1=$(sbatch -C "sirocco&p100" ./test-hwloc-single.sh $tarballname | sed "s#Submitted batch job ##")
echo "$job1"

echo "Submitting job ..."
job2=$(sbatch -C "sirocco&v100" ./test-hwloc-single.sh $tarballname | sed "s#Submitted batch job ##")
echo "$job2"

echo "Submitting job ..."
job3=$(sbatch -C "sirocco" ./test-hwloc-single.sh $tarballname | sed "s#Submitted batch job ##")
echo "$job3"

echo "Waiting for all jobs to finish..."
sbatch --wrap /bin/true --wait --dependency afterany:$job1:$job2:$job3

echo "Dumping jobs output:"

echo "####################################################"
echo "###################### $job1 #######################"
echo "####################################################"
cat slurm-${job1}.out
if tail -1 slurm-${job1}.out | grep "SUCCESS" >/dev/null ; then
  rm -f slurm-${job1}.out
else
  failedlist="$failedlist $job1"
fi

echo "####################################################"
echo "###################### $job2 #######################"
echo "####################################################"
cat slurm-${job2}.out
if tail -1 slurm-${job2}.out | grep "SUCCESS" >/dev/null ; then
  rm -f slurm-${job2}.out
else
  failedlist="$failedlist $job2"
fi

echo "####################################################"
echo "###################### $job3 #######################"
echo "####################################################"
cat slurm-${job3}.out
if tail -1 slurm-${job3}.out | grep "SUCCESS" >/dev/null ; then
  rm -f slurm-${job3}.out
else
  failedlist="$failedlist $job3"
fi


echo "#################################################"
echo "##################### summary ###################"
echo "#################################################"

echo -n $test_id > $test_id_file

if test -n "$failedlist"; then
  echo "Failed jobs: $failedlist"
  exit 1
else
  echo "All jobs succeeded"
  exit 0
fi
