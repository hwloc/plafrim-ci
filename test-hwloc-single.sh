#!/bin/sh

set -e

echo "############################"
echo "#### Running on "`hostname`
echo "############################"

dir=$(mktemp -d /tmp/test-hwloc.XXXXXXXX)
cd $dir

tarballname=$1

if test x$tarballname = x; then
  echo "Failed to get tarball name."
  exit 1
fi

wget "https://ci.inria.fr/hwloc/job/basic/job/master/lastSuccessfulBuild/artifact/$tarballname"

if ! test -f "$tarballname"; then
  echo "Failed to download tarball."
  exit 1
fi

basename=$(basename "$tarballname" .tar.bz2)

tar xf "$tarballname"
cd "$basename"

mkdir build
cd build

# load a module that matches the CUDA version (some aren't compatible)
cudaversion=$(nvidia-smi -q | grep CUDA | awk {'print $4'})
module load compiler/cuda/$cudaversion

../configure --enable-cuda --enable-nvml --enable-opencl

make -j

make check

utils/lstopo/lstopo-no-graphics

nrcuda=$(utils/lstopo/lstopo-no-graphics --only osdev | grep \"cuda | wc -l)
nrnvml=$(utils/lstopo/lstopo-no-graphics --only osdev | grep \"nvml | wc -l)
nropencl=$(utils/lstopo/lstopo-no-graphics --only osdev | grep \"opencl0d | wc -l)
nrpcinvidia=$(utils/hwloc/hwloc-info -s pci[10de:]:all | wc -l)

echo "Found $nrcuda CUDA OSdevs, $nrnvml NVML OSdevs, $nropencl OpenCL OSdevs for $nrpcinvidia NVIDIA PCI devs"

if test $nrcuda != $nrpcinvidia; then
  echo "Number of CUDA OSdevs is invalid"
  failed=1
fi
if test $nrnvml != $nrpcinvidia; then
  echo "Number of NVML OSdevs is invalid"
  failed=1
fi
if test $nropencl != $nrpcinvidia; then
  echo "Number of OpenCL OSdevs is invalid"
  failed=1
fi

cd ..

cd ..
rm -rf "$basename" "$tarballname"

cd
rmdir $dir

if test x$failed = x1; then
  exit 1
fi

echo "$0 SUCCESS"
exit 0
